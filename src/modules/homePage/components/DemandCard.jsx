/**
 * Knowledge In Demand Card Component
 * */
import { Card, Avatar,Button } from 'antd'
import styled from 'styled-components';
import React from "react";
import "../css/home.scss"
const { Meta } = Card;
export const DemandCard = ({ _id, imageUrl, avatar, category, title, stars, times, viewCounts,props }) => {
  return (
    <ShadowCard
      hoverable
      
      cover={<Image alt={title} src={imageUrl} />}
    >
      <Meta
        title={
          <MetaTitle
            avatar={avatar}
            category={category}
            title={title}
            times={times}
            averageStar={stars}
            viewCounts={viewCounts}
            props={props}
          />
        }
      />
    </ShadowCard>
  )
}

const MetaTitle = ({ avatar, category, title, averageStar, times, viewCounts,props }) => {
  return (
    <MetaTitleContainer>
      <DescriptionContainer>
        <div className="display-div-justify-content">
          <div>
            <div className="meta-money"><span>$120</span></div>
            <div className="meta-money-product-name"><span>产品名称</span></div>
          </div>
          <div>
            <Button className="supermarket-name-button">超市名称</Button>
          </div>
        </div>

      </DescriptionContainer>
    </MetaTitleContainer>
  )
}

const Image = styled.img`
  width:100%;
  object-fit: cover;
  height:200px;
`
const ShadowCard = styled(Card)`
  width:100%;
  background:rgba(255,255,255,1);
  box-shadow:3px 3px 6px rgba(0,0,0,0.16);
  border-radius:20px;
  margin-bottom:10px;
  .ant-card-cover img{
    border-radius:20px 20px 0px 0px;
  }
`

const DescriptionContainer = styled.div`
  padding-left:10px;
  width: calc(100% - 50px);
`

const MetaTitleContainer = styled.div`
  display:flex;
  width:100%;
`
