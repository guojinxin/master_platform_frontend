import React, { Component } from 'react';
import { DemandCard } from "./DemandCard";
import { Col, Row } from "antd";
import styled from "styled-components";
import { Link, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators, compose } from "redux";
const productData = [{
  coverUrl: require('../assets/timg-11.png'),
  category: "1111111",
  courseName: "haode",
  times: "2020-06-22",
  averageStar: 2
},
{
  coverUrl: require('../assets/timg-11.png'),
  category: "1111111",
  courseName: "haode",
  times: "2020-06-22",
  averageStar: 2
},
{
  coverUrl: require('../assets/timg-11.png'),
  category: "1111111",
  courseName: "haode",
  times: "2020-06-22",
  averageStar: 2
},
{
  coverUrl: require('../assets/timg-11.png'),
  category: "1111111",
  courseName: "haode",
  times: "2020-06-22",
  averageStar: 2
},
{
  coverUrl: require('../assets/timg-11.png'),
  category: "1111111",
  courseName: "haode",
  times: "2020-06-22",
  averageStar: 2
},
{
  coverUrl: require('../assets/timg-11.png'),
  category: "1111111",
  courseName: "haode",
  times: "2020-06-22",
  averageStar: 2
}]
class _Recommend extends Component {


  render() {
    return (
      <DemandContainer>
        <Title>推荐商品</Title>
        <Row style={{ width: "100%" }} type={"flex"} justify={'center'} align={"middle"}>
          <Col xs={23} sm={23} md={23} lg={24} xl={24}>
            <Row style={{ width: "100%" }}>
              {productData.map((pane, index) => {
                const { coverUrl, category, courseName, times, averageStar } = pane;
                return (
                  <Col xs={24} sm={24} md={12} lg={8} xl={8} xxl={6} key={index} style={{ padding: "0 7px 10px" }}>
                    <Link to={`/productDetail`}>
                      <DemandCard
                        avatar={'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png'}
                        imageUrl={coverUrl}
                        category={category}
                        title={courseName}
                        stars={averageStar}
                        times={times}
                        viewCounts={0}
                        props={this.props}
                      />
                    </Link>
                  </Col>
                )
              })}
            </Row>
          </Col>
        </Row>
      </DemandContainer>
    );
  }
}


const mapStateToProps = (state) => {
  return {
  }
}
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({

  }, dispatch);
}
const Components = compose(
  connect(mapStateToProps, mapDispatchToProps)
)(_Recommend)
export const Recommend = withRouter(Components);
const DemandContainer = styled.div`
  width:100%;
  display:flex;
  justify-content:center;
  flex-direction:column;
`
const Title = styled.div`
  color:#333;
  display:flex;
  justify-content:center;
  font-size:24px;
  margin-top:88px;
  margin-bottom:15px;
  font-weight:400;
  font-weight:bold;
  display: flex;
  justify-content: end;
  padding-left: 10px;
`