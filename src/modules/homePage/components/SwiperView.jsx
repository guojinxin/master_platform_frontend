import React, {Component} from 'react';
import {SwiperComponent} from "../../../components/CommonComponents/HomeSwiper";
import {BannerCard} from "../../../components/CommonComponents/BannerCard";
import styled from "styled-components";
import {bindActionCreators, compose} from "redux";
import {connect} from "react-redux";
import _ from "lodash"
const bannerData = [{
  title:"hhhhhh",
  imageUrl:require("../assets/timg-9.png"),
  description:"descriptiondescriptiondescriptiondescriptiondescription",
  link:"www.baidu.com"
},{
  title:"hhhhhh",
  imageUrl:require("../assets/timg-9.png"),
  description:"descriptiondescriptiondescriptiondescriptiondescription",
  link:"www.baidu.com"
},{
  title:"hhhhhh",
  imageUrl:require("../assets/timg-9.png"),
  description:"descriptiondescriptiondescriptiondescriptiondescription",
  link:"www.baidu.com"
}]

export class _SwiperView extends Component {
  
  render() {
    return (
          <SwiperContainer>
            <SwiperComponent
              swiperId={"home_swiper"}
              isShowPagination={false}
              onSlideChange={() => {
              }}
              swiperData={bannerData}
              renderItem={(item, index) => {
                return (
                  <BannerCard key={index} category={item.title} cover={item.imageUrl} title={item.description} link={item.link}/>
                )
              }}
            />
          
          </SwiperContainer>

    );
  }
}
const mapStateToProps = (state) => {
  return {
    
  }
}
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
  }, dispatch);
}
const Components = compose(
  connect(mapStateToProps, mapDispatchToProps)
)(_SwiperView)
export const SwiperView = Components
const SwiperContainer = styled.div`
  width:100%;
`
