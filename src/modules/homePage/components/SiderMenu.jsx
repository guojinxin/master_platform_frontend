import React, {Component} from 'react';
import {Avatar, Button, Form, Icon, Input, Layout, Menu, notification, Upload, Spin} from 'antd';
import styled from "styled-components";
import {connect} from "react-redux";
import {Link, withRouter} from 'react-router-dom';
import {END_POINT, PRIMARY_COLOR} from "../../../utils/constants";
import {beforeUploadAvatar} from "../../../utils/beforeUploadUtil";
import _, {isEmpty} from "lodash"

const {Sider} = Layout;

function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener("load", () => callback(reader.result));
  reader.readAsDataURL(img);
}

class _SiderMenu extends Component {
  constructor(props) {
    super(props)
    this.state = {
      avatar: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
      collapsed: true,
      editable: false,
      userName: "Jinxin",
    }
  }


  onCollapse = () => {
    this.setState({collapsed: !this.state.collapsed});
  };

  componentDidMount() {
    this.props.getSiderComponent(this);
  }

  onClickEdit = () => {
    this.setState({
      editable: true
    }, () => {
      this.props.form.setFieldsValue({
        userName: this.state.userName
      })
    })
  }
  openNotification = ({message, description}) => {
    notification.error({
      message,
      description,
      placement: "topLeft",
      closeIcon: <span/>
    });
  };

  avatarChange = info => {
    if (info.file.status === "uploading") {
      this.setState({loading: true});
      return;
    }
    if (info.file.status === "done") {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, imageUrl =>
          this.props.getUserAvatar(info.file.response.data),
        this.setState({
          avatar: info.file.response.data.avatar,
        })
      );
    }
  };

  static getDerivedStateFromProps(nextProps, prevState) {
   
    return null
  }

  // 用户名修改
  setUserName = () => {
    // 首先先关闭当前的这个修改输入框，然后下面去做网络请求
    this.setState({editable: false,});
    this.props.form.validateFieldsAndScroll(async (err, values) => {
      if (!err) {
        const {userName} = values;
        // 用户名如果没有变化则不做任何操作
        if (userName === this.state.userName) {
          return
        }
        this.setState({
          userName
        })
        try {
         
        } catch (e) {
          this.openNotification({message: "Reset User Name error", description: e.message});
          this.setState({editable: true}, () => {
            this.props.form.setFieldsValue({
              userName,
            })
          })
        }
      }
    })
  };

  validateUserName = (rule, value, callback) => {
    if (!value) {
      callback()
    }
    if (value.length < 3) {
      callback("Name should be no less than 3 characters")
    }
    const testUserName = /[~!@#$%^&*()/\|,.<>?"'();:+-=\[\]{}]/;//只能输入汉字和英文字母
    if (testUserName.test(value)) {
      callback("Name can only contain letters and '_'")
    }
    callback()
  }

  render() {
    
   
    const {editable, avatar} = this.state;
    const {getFieldDecorator} = this.props.form
    return (
        <CustomSider
          collapsible
          width={300}
          defaultCollapsed={true}
          collapsed={this.state.collapsed}
          collapsedWidth={0}
          trigger={null}
          theme={'light'}
        >
          <SiderContainer>
            <CloseContainer>
              <CloseIcon type={"close"} onClick={this.onCollapse}/>
            </CloseContainer>
           
              <UserContainer>
                <Upload
                  name="file"
                  showUploadList={false}
                  action={`${END_POINT}/user/AvatarUpload`}
                  headers={{
                    Authorization: localStorage.getItem(
                      "token"
                    )
                  }}
                  multiple={false}
                  accept={"image/*"}
                  beforeUpload={(file) => beforeUploadAvatar({file, openModal: this.props.openModal})}
                  onChange={this.avatarChange}
                >
                  <UserAvatar src={avatar}/>
                </Upload>

                <UserNameContainer>
                  {editable ?
                    <Form style={{width: "inherit"}}>
                      <Form.Item>
                        {getFieldDecorator('userName', {
                          rules: [
                            {
                              required: true,
                              message: 'Please input your user name',
                            },
                            {
                              validator: this.validateUserName,// 验证用户名
                            },
                          ],
                        })(
                          <UserNameInput
                            //allowClear
                            maxLength={18}
                            onBlur={this.setUserName}
                            onPressEnter={this.setUserName}
                          />
                        )}
                      </Form.Item>
                    </Form> :
                    <EditContainer>
                      <UserName>{this.state.userName}</UserName>
                      <EditIcon type={"edit"} onClick={this.onClickEdit}/>
                    </EditContainer>
                  }
                </UserNameContainer>
              </UserContainer>
             
            <Menu mode="inline">
              <Menu.Item key="Course">
                <Link to={"/myCourse"}>
                  <IconImage src={require("../assets/order.png")}/>
                  <MenuText>My Order</MenuText>
                </Link>
              </Menu.Item>
              {/*
              <Menu.Item key="PersonalJourney">
                <Link to={"/personalJourney"}>
                  <IconImage src={require("./assets/icon_journey.png")}/>
                  <MenuText>Personal Journey</MenuText>
                </Link>
              </Menu.Item>
*/}
              <Menu.Item key="AccountSetting">
                <Link to={"/userSetting"}>
                  <IconImage src={require("../assets/icon_setting.png")}/>
                  <MenuText>Setting</MenuText>
                </Link>
              </Menu.Item>
              {/*<Menu.Item key="Friends">
              <Link to={"/friends"}>
                <IconImage src={require("./assets/icon_friends.png")} />
                <MenuText>Friends</MenuText>
              </Link>
            </Menu.Item>*/}
             {localStorage.getItem("savants_username") === "1907745233@qq.com"?
              <Menu.Item key="UploadCourse">
                <Link to={_.isEmpty(localStorage.getItem("token")) ? "/signIn" : "/uploadCourse"}>
                  <IconImage src={require("../assets/icon_upload.png")}/>
                  <MenuText>Upload Course</MenuText>
                </Link>
              </Menu.Item>:<div></div>}
              <Menu.Item key="AccountSetting">
                <Link to={"/accountSetting/myUpload"}>
                <IconImage src={require("../assets/lishi.png")}/>
                  <MenuText>History</MenuText>
                </Link>
              </Menu.Item>
             
            </Menu>
          </SiderContainer>
        </CustomSider>
    );
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

export const SiderMenu = connect(mapStateToProps, {
  
})(withRouter(Form.create({name: "userName"})(_SiderMenu)));
const CustomSider = styled(Sider)`
  background:linear-gradient(180deg,rgba(255,196,0,1) 0%,rgba(243,115,53,1) 100%);
  box-shadow:3px 0px 6px rgba(0,0,0,0.16);
  .ant-menu{
    background:transparent;
  }
  .ant-menu-item{
    display:flex;
    align-items:center;
    margin-top:20px;
  }
  .ant-menu-item-selected{
    background:${PRIMARY_COLOR} !important;
  }
  .ant-menu-inline{
    border-right: 0;
  }
  .ant-menu-item::after{
    display:none;
  }
`
const MenuText = styled.span`
  color:#fff;
  font-size:18px;
  font-weight:400;
  margin-left:20px;
`
const MenuHomeText = styled.span`
  color:#fff;
  font-size:18px;
  font-weight:400;
  margin-left:10px;
`
const SiderContainer = styled.div`
  width:100%;
`
const UserAvatar = styled(Avatar)`
  height:80px;
  width:80px;
  border-radius:50%;
`
const UserContainer = styled.div`
  width:100%;
  display:flex;
  justify-content:center;
  align-items:center;
  flex-direction:column;
`

const UserName = styled.div`
  font-size:25px;
  font-weight:400;
  color:#fff;
`
const IconImage = styled.img`
  height:24px;
  width:24px;
`
const CloseContainer = styled.div`
  display:flex;
  justify-content:flex-end;
  padding-right:20px;
  margin-top:60px;
  margin-bottom:20px;
`
const CloseIcon = styled(Icon)`
  color:#fff;
  font-size:28px;
  cursor:pointer;
`
const SignButton = styled(Button)`
  background:rgba(255,255,255,1);
  box-shadow:2px 2px 5px rgba(0,0,0,0.16);
  border-radius:30px;
  border:0;
  margin:0 10px;
`
const ButtonText = styled.span`
  color: #333;
  font-size:18px;
  padding:0 10px;
`
const SignContainer = styled.div`
  display:flex;
  justify-content:space-between;
  padding:10px 20px 0;
`
const EditIcon = styled(Icon)`
  font-size:20px;
  color:#fff;
  margin-left:15px;
  cursor:pointer;
`
const UserNameContainer = styled.div`
  display:flex;
  width:100%;
  padding:0 12px;
  align-items:center;
  justify-content:center;
  margin-top:25px;
  .ant-form-explain{
     margin-top: 10px;
  }
`
const UserNameInput = styled(Input)`
  height:46px;
  .ant-input{
    font-size:22px;
  }
`
const HomeLink = styled(Link)`
  display: flex !important;;
  align-items: center !important;
  margin-left: 2px;
  .ant-menu-item .anticon, .ant-menu-submenu-title .anticon {
    min-width: 14px;
    margin-left: 2px !important;;
    font-size: 14px;
    -webkit-transition: font-size 0.15s cubic-bezier(0.215, 0.61, 0.355, 1), margin 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);
    transition: font-size 0.15s cubic-bezier(0.215, 0.61, 0.355, 1), margin 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);
  }
`
const EditContainer = styled.div`
  display:flex;
  align-items:center;
  height:46px;
  margin-bottom:24px;
`
