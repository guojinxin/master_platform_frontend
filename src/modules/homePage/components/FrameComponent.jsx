import React from "react";
import {Col, Icon, Layout} from "antd";
import styled from "styled-components";
import {SearchInput} from "./SearchInput";
import {SiderMenu} from "./SiderMenu";
import '../css/sider.scss';
import _ from "lodash"
import {HeaderContent} from "./HeaderContent"
const {Header, Content} = Layout;

export class FrameComponent extends React.Component {
  state = {
    iconBackground: "primary",
  }
  onOpenSider = () => {
    this.sider.onCollapse()
  }
  getSiderComponent = (component) => {
    this.sider = component
  }

  componentDidMount() {
    const {needChangeIconColor = false} = this.props;
    if (needChangeIconColor) {
      this.setState({
        iconBackground: "white"
      })
    }
  }

  onScrollCapture = () => {
    const {needChangeIconColor = false} = this.props;
    const headerMain = document.getElementById("custom-header");
    if (!needChangeIconColor) {
      headerMain.classList.add("headerMain-bg");
      return
    }
    const offset = this.content.scrollTop;

    if (headerMain) {
      if (offset >= 60) {
        headerMain.classList.add("headerMain-bg");
        this.setState({iconBackground: 'primary'});
      } else {
        this.setState({iconBackground: 'white'});
        headerMain.classList.remove("headerMain-bg");
      }
    }
  }

  render() {
    const {iconBackground} = this.state;
    return (
      <MainLayout>
        <SiderMenu getSiderComponent={this.getSiderComponent}/>
        <ContentLayout>
        <CustomHeader id={'custom-header'}>
            <MenuIcon
              type={"menu"}
              onClick={this.onOpenSider}
              style={{color: "#F5AF19"}}
            />
            <HeaderContent></HeaderContent>
          </CustomHeader>
          <CustomContent ref={refs => this.content = refs} onScrollCapture={this.onScrollCapture}>
            {this.props.children}
          </CustomContent>
        </ContentLayout>
      </MainLayout>
    );
  }
}

export const ContentFrame = ({children}) => {
  return (
    <Col style={{marginTop:"160px"}} xs={24} sm={23} md={22} lg={19} xl={18}>
      {children}
    </Col>
  )
}
export const Title = styled.span`
  color:#333;
  font-size:36px;
  margin:100px 15px 50px;
  text-align:center;
  font-weight:bold;

`

const MenuIcon = styled(Icon)`
  font-size:28px;
  color:#fff;
  cursor: pointer;
`

const MainLayout = styled(Layout)`
  height:100vh;
  width:100vw;
  background:#fff
`

const ContentLayout = styled(Layout)`
  height:100vh;
  position:relative;
  background:#fff
`
const CustomHeader = styled(Header)`
  display:flex;
  align-items:center;
  position: absolute;
  z-index: 9998;
  width: 100%;
  background: rgba(255,255,255,0);
  transaction: all 0.3s;
  padding-top: 50px;
  padding-bottom: 50px;
`
const CustomContent = styled.div`
  height:100vh;
  padding-bottom:30px;
  overflow-y: scroll;
  background:#fff;
  display:flex;
  flex-direction:column;
  align-items:center;
  background:#fff;
`

