import React, { Component } from 'react';
import styled from "styled-components";
import { Input } from "antd";
import { withRouter } from "react-router-dom";
import { compose, bindActionCreators } from "redux";
import { connect } from "react-redux";
import _ from "lodash"
class _SearchInput extends Component {
  state = {
    focused: false,
    content: ""
  }
  onClickSearchIcon = () => {
    this.searchInput.focus()
  }
  searchInputOnBlur = () => {
    this.setState({ focused: false })
  }
  searchInputOnFocus = () => {
    this.setState({ focused: true })
  }
  searchChange = e => {
    this.setState({ content: e.target.value })
  }
  render() {
    const { iconBackground } = this.props;
    const searchIcon = require("../assets/icon_search.png")
    return (
      <SearchContainer>
        <SearchIcon src={searchIcon} onClick={this.onClickSearchIcon} />
        <div className={["search-input-container", this.state.focused ? "search-input" : ""].join(" ")}>
          <CustomSearch
            ref={refs => this.searchInput = refs}
            placeholder=""
            onChange={this.searchChange}
            onBlur={this.searchInputOnBlur}
            onFocus={this.searchInputOnFocus}
            value={this.state.content}
            onPressEnter={() => {
              this.props.history.push('/searchedCourse')


            }}
          />
        </div>
      </SearchContainer>
    );
  }
}
function mapStateToProps(state) {
  return {

  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
  }, dispatch)
}

const Components = compose(
  connect(mapStateToProps, mapDispatchToProps)
)(_SearchInput)
export const SearchInput = withRouter(Components)
const CustomSearch = styled(Input)`
  background:transparent;
  color:#222;
  border:0 !important;
  border-bottom:1px solid #ff8900 !important;
  border-radius:0;
`
const SearchIcon = styled.img`
  height:30px;
  width:30px;
  cursor: pointer;
  margin-right:16px;
`
const SearchContainer = styled.div`
  display:flex;
  align-items:center;
  .search-input-container{
  transition: width .10s linear;
  outline: none;
  width: 0 !important;
  overflow: hidden;
  input::-webkit-input-placeholder {
    color: #666;
  }
  .ant-input:focus {
    border: none;
    box-shadow: none;
  }
}
.search-input{
  width:200px !important;
}

`
