import React, { Component } from 'react';
import styled from "styled-components";
import { Input } from "antd";
import { withRouter } from "react-router-dom";
import { compose, bindActionCreators } from "redux";
import { connect } from "react-redux";
import _ from "lodash"
import "../css/home.scss"
class _HeaderContent extends Component {
  render() {
    return (
      <div className="header-content-div">
        <div className="header-span-logo">
          <span>Logo</span>
        </div>
        <div className="header-content-margin-left">
          <img alt="logo" src={require("../assets/weizhicopy.png")}></img>
          <span className="header-span-location">location</span>
        </div>
        <div className="header-content-margin-left-input">
          <Input className="header-input-search" placeholder="search"></Input>
          <img alt="sousuo" src={require("../assets/sousuo-2.png")} className="sousuo-img-header-input-search"></img>
        </div>
        <div className="header-content-margin-image-sousou">
          <img  alt="sousuo" src={require("../assets/gouwuche.png")}></img>
          <div className="header-content-margin-register">Log in / <span className="home-cursor-pointer" onClick={() => {
            this.props.history.push('')
          }}>Register</span></div>
        </div>
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {

  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
  }, dispatch)
}

const Components = compose(
  connect(mapStateToProps, mapDispatchToProps)
)(_HeaderContent)
export const HeaderContent = withRouter(Components)
