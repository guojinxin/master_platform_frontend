import {CLOSE_MODAL, OPEN_MODAL} from "../types";

const initialState = {
  modalVisible: false,
  modalContent: null,
  modalTitle: "",
}
export const modalReducer = (state = initialState, action) => {
  switch (action.type) {
    case OPEN_MODAL:
      return {
        ...state,
        modalVisible: true,
        modalContent: action.payload.content,
        modalTitle: action.payload.title
      }
    case CLOSE_MODAL:
      return {
        ...initialState,
      }
    default:
      return state
  }
}
