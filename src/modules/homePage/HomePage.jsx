import React, { Component } from 'react';
import { Col } from 'antd'
import _ from "lodash"
import { compose } from "redux";
import { connect } from "react-redux";
import { SwiperView } from "./components/SwiperView";
import { Recommend } from "./components/Recommend";
export const ContentFrame = ({ children }) => {
  return (
    <Col style={{ marginTop: "160px" }} xs={24} sm={23} md={22} lg={19} xl={18}>
      {children}
    </Col>
  )
}
export class _HomePage extends Component {
  render() {
    return (
      <ContentFrame>

        <SwiperView />
        <Recommend />
      </ContentFrame>

    );
  }
}
const mapStateToProps = (state) => {
  return {
    // LastClass: state.LastClass,
    // createCourse: state.createCourse,
    // putawayCourse: state.putawayCourse,
    // banner: state.banner,
  }
}

const Components = compose(
  connect(mapStateToProps, {})
)(_HomePage)
export const HomePage = Components
