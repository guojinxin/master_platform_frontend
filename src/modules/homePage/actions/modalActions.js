import {CLOSE_MODAL, OPEN_MODAL} from "../types";

export const openModal = ({content, title}) => {
  return dispatch => {
    dispatch({
      type: OPEN_MODAL,
      payload: {content, title},
    })
  }
}

export const closeModal = () => {
  return dispatch => {
    dispatch({
      type: CLOSE_MODAL,
    })
  }
}
