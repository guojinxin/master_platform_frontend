import { END_POINT } from "../../../utils/constants";
import axios from "axios";
export const getHomeApi = async ({ companyId, name }) => {
  return await axios.get(`${END_POINT}/api/employee/${companyId}/getEmployee`, {
    params: { name },
  });
};
