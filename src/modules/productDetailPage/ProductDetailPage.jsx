import React, { Component } from 'react';
import { Row, Col, Spin } from "antd";
import styled from 'styled-components';
import _ from "lodash"
import { ProductDetailTop } from "./components/ProductDetailTop";
import {ProductTabs} from "./components/ProductTabs";
export const ContentFrame = ({ children }) => {
  return (
    <Col xs={24} sm={24} md={24} lg={24} xl={24}>
      {children}
    </Col>
  )
}
export class ProductDetailPage extends Component {
  render() {
    return (
      <ContentContainer>
        <ContentFrame>
          <ProductDetailTop />
          <ProductTabs/>
        </ContentFrame>
      </ContentContainer>
    );
  }
}

const BlurImage = styled.img`
  width:110%;
  height:100%;
  margin-left:-5%;
  margin-right:-5%;
  margin-top: -20px;
  filter: url(blur.svg#blur);
  filter: blur(20px);
  filter: progid:DXImageTransform.Microsoft.Blur(PixelRadius=10, MakeShadow=false);
`
const ImageContainer = styled.div`
  width:100%;
  height:820px;
  position:relative;
  overflow: hidden;
  top:0;
`
const ContentContainer = styled.div`
  position: relative;
  display:flex;
  justify-content:center;
  width: 100%;
`
const Container = styled.div`
  width:100%;
  height: inherit;
`
