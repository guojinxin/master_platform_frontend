import {
  GET_HOME_COURSES_START,
  GET_HOME_COURSES_SUCCESS,
  GET_HOME_COURSES_FAILURE,
} from "../types";
import {
  getHomeApi
} from "../api/index";
import _ from "lodash";
import {message} from 'antd';
import {MessageContent, MessageError, MessageSuccess} from "../../../components/CommenComponents/MessageContent";
import React from "react";
export const getHomePage = () => {
  return async (dispatch) => {
    dispatch({
      type: GET_HOME_COURSES_START,
    });
    try {
      const result = await getHomeApi({});
      const { data, status } = result;
      if (status === 200 && !_.isEmpty(data.data)) {
        dispatch({
          type: GET_HOME_COURSES_SUCCESS,
          payload: data.data,
        });
        message.success({
          icon: <span/>,
          content: <MessageContent><MessageSuccess>modify successfully!</MessageSuccess></MessageContent>,
        })
      }
    } catch (e) {
      dispatch({
        type: GET_HOME_COURSES_FAILURE,
      });
      message.error({
        icon: <span/>,
        content: <MessageContent><MessageError>{e.message}</MessageError></MessageContent>,
      })
    }
  };
};