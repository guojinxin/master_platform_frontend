import React, {Component} from 'react';
import {Row, Col} from 'antd';
import styled from 'styled-components';
import "../css/index.scss";
import {OwnerInfo} from "./OwnerInfo"
export const CenterCol = styled(Col)`
  display: flex;
  align-items:center;
  justify-content:center;
  flex-direction:column;
  padding-left:2%;
  padding-right:2%
  
`
export class ProductDetailTop extends Component {
  render() {
    return (
      <CourseDetailTopContainer>
        <CenterCol xs={24} sm={24} md={20} lg={20} xl={20}>
          <Row style={{width:"100%",marginTop:"160px"}}>
            <CenterCol xs={23} sm={24} md={24} lg={12} xl={12}>
              <img className="product-img" alt="img" src={require('../assets/timg-12.png')}/>
            </CenterCol>
            <OwnerInfoCol xs={23} sm={24} md={24} lg={12} xl={12}>
              <OwnerInfo/>
            </OwnerInfoCol>
          </Row>
        </CenterCol>
      </CourseDetailTopContainer>
    );
  }
}

const CourseDetailTopContainer = styled.div`
  display:flex;
  width:100%;
  justify-content:center;
`
const OwnerInfoCol = styled(Col)`
  padding-left:2%;
  padding-right:2%;
`
