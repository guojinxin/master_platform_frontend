import React, {Component} from 'react';
import styled from 'styled-components';
import {CommentCard} from "../../../components/CommonComponents/CommentCard";
import moment from "moment";
import {bindActionCreators, compose} from "redux";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";
import { List } from "antd";
const productData = [{
  reviewMessage:"reviewMessagereviewMessagereviewMessagereviewMessage",
  star:5,
  publishedAt:"2020-06-21"
},{
  reviewMessage:"reviewMessagereviewMessagereviewMessagereviewMessage",
  star:5,
  publishedAt:"2020-06-21"
},{
  reviewMessage:"reviewMessagereviewMessagereviewMessagereviewMessage",
  star:5,
  publishedAt:"2020-06-21"
},{
  reviewMessage:"reviewMessagereviewMessagereviewMessagereviewMessage",
  star:5,
  publishedAt:"2020-06-21"
}]
export class _Comment extends Component {
  render() {
    return (
      <div style={{width: "100%"}}>
        
        <List
          dataSource={productData}
          pagination={{
            defaultCurrent: 1,
          }}
          renderItem={(comment) => {
            const userInfo = {
              avatar:"https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png",
              userName:"jinxin",
              userId:"1907745233@qq.com",
              introduce:"introduceintroduceintroduceintroduceintroduceintroduceintroduce",
            }
            return (
              <CommentCard
                commentContent={comment.reviewMessage}
                score={comment.star}
                userInfo={userInfo}
                publishedAt={moment(comment.createdAt).toDate()}
                style={{marginBottom: '15px'}}
              />
            )
          }}
        />

      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
   
  }
}
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    
  }, dispatch);
}
const Components = compose(
  connect(mapStateToProps, mapDispatchToProps)
)(_Comment)
export const Comment = withRouter(Components);

const Title = styled.div`
  color:#333;
  font-size:24px;
  font-weight:bold;
`
const TitleContainer = styled.div`
  display:flex;
  align-items:center;
  margin-bottom:38px;
  margin-top:38px;
`

const StarContainer  = styled.div`
  margin-left: 50px;
`
