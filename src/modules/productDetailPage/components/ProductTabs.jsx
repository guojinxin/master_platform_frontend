import React, { Component } from 'react';
import styled from 'styled-components';
import { Tabs, Col,Row } from 'antd';
import { Link, withRouter } from "react-router-dom";
import {Comment} from "./Comment";
import { DemandCard } from "../../homePage/components/DemandCard";

const { TabPane } = Tabs;

function callback(key) {
  console.log(key);
}
const productData = [{
  coverUrl: require('../assets/timg-11.png'),
  category: "1111111",
  courseName: "haode",
  times: "2020-06-22",
  averageStar: 2
},
{
  coverUrl: require('../assets/timg-11.png'),
  category: "1111111",
  courseName: "haode",
  times: "2020-06-22",
  averageStar: 2
},
{
  coverUrl: require('../assets/timg-11.png'),
  category: "1111111",
  courseName: "haode",
  times: "2020-06-22",
  averageStar: 2
},
{
  coverUrl: require('../assets/timg-11.png'),
  category: "1111111",
  courseName: "haode",
  times: "2020-06-22",
  averageStar: 2
},
{
  coverUrl: require('../assets/timg-11.png'),
  category: "1111111",
  courseName: "haode",
  times: "2020-06-22",
  averageStar: 2
},
{
  coverUrl: require('../assets/timg-11.png'),
  category: "1111111",
  courseName: "haode",
  times: "2020-06-22",
  averageStar: 2
}]
class _ProductTabs extends Component {
  render() {
    return (
      <CourseDetailTopContainer>
        <CenterCol xs={24} sm={24} md={20} lg={20} xl={20}>
          <Tabs className="product-tabs" defaultActiveKey="1" onChange={callback}>
            <TabPane tab="评价" key="1">
              <Row type={"flex"} justify={'center'}>
                <CenterCol xs={24} sm={24} md={24} lg={24} xl={24}>
                  <Comment/>
                </CenterCol>
              </Row>
            </TabPane>
            <TabPane tab="相关产品推荐" key="2">
            <Row style={{ width: "100%" }} type={"flex"} justify={'center'} align={"middle"}>
          <Col xs={23} sm={23} md={23} lg={24} xl={24}>
            <Row style={{ width: "100%" }}>
              {productData.map((pane, index) => {
                const { coverUrl, category, courseName, times, averageStar } = pane;
                return (
                  <Col xs={24} sm={24} md={12} lg={8} xl={8} xxl={6} key={index} style={{ padding: "0 7px 10px" }}>
                    <Link to={`/productDetail`}>
                      <DemandCard
                        avatar={'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png'}
                        imageUrl={coverUrl}
                        category={category}
                        title={courseName}
                        stars={averageStar}
                        times={times}
                        viewCounts={0}
                        props={this.props}
                      />
                    </Link>
                  </Col>
                )
              })}
            </Row>
          </Col>
        </Row>
            </TabPane>

          </Tabs>
        </CenterCol>
      </CourseDetailTopContainer>
    );
  }
}

export const ProductTabs = withRouter(_ProductTabs);
export const CenterCol = styled(Col)`
  display: flex;
  align-items:center;
  justify-content:center;
  flex-direction:column;
  padding-left:2%;
  padding-right:2%
  
`
const CourseDetailTopContainer = styled.div`
  display:flex;
  width:100%;
  justify-content:center;
`