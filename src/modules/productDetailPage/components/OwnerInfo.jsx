import React, { Component } from 'react';
import styled from 'styled-components';
import { Avatar, Row, Button, Tag } from "antd";
import { MinusOutlined, PlusOutlined } from '@ant-design/icons';

import { withRouter } from "react-router-dom";

class _OwnerInfo extends Component {
  state = {
    percent: 1,
  };

  increase = () => {
    let percent = this.state.percent + 1;
    if (percent > 100) {
      percent = 100;
    }
    this.setState({ percent });
  };

  decline = () => {
    let percent = this.state.percent - 1;
    if (percent < 1) {
      percent = 1;
    }
    this.setState({ percent });
  };
  render() {
    return (
      <OwnerInfoContainer>
        <div className="owner-info-product-name">产品名称</div>
        <div className="owner-info-product-div">
          <span className="owner-info-product-money">$120</span>
          <span className="owner-info-product-freight-money">运费：$ 5</span>
          <Distance>{"超市名称"}</Distance>
        </div>
        <div className="owner-info-product-div-describe">细节，产地，名称</div>
        <div className="owner-info-product-please-choose">请选择</div>
        <div className="weight-tag-div">
          <WeightTag>50 g</WeightTag>
          <WeightTag>80 g</WeightTag>
          <WeightTag>100 g</WeightTag>
          <WeightTag>150 g</WeightTag>
          <WeightTag>1000 g</WeightTag>
          <WeightTag>2000 g</WeightTag>
        </div>
        <div className="owner-info-product-count">数量</div>
        <div style={{marginTop:"20px"}}>
          <CountTag onClick={this.decline}>-</CountTag>
          <span className="owner-info-product-percent">{this.state.percent}</span>
          <CountTag onClick={this.increase}>+</CountTag>
        </div>
        <div style={{marginTop:"20px"}}>
          <Button className="owner-info-product-cart">加入购物车</Button>
          <Button className="owner-info-product-buy">直接购买</Button>
        </div>
      </OwnerInfoContainer>
    );
  }
}

export const OwnerInfo = withRouter(_OwnerInfo);

const OwnerInfoContainer = styled.div`
  .description-all {
    height:auto
  }
`

const Distance = styled(Tag)`
  width:100px;
  height:20px;
  background:rgba(245,175,25,1);
  opacity:1;
  border-radius:12px;
  font-size:12px;
  font-family:Arial;
  color:rgba(255,255,255,1);
  text-align: center;
  margin-left: 40%;
`
const WeightTag = styled(Tag)`
  width:120px;
  height:30px;
  background:rgba(255,255,255,1);
  border:1px solid rgba(241,241,241,1);
  opacity:1;
  margin-right:13px;
  text-align: center;
  padding-top:3px;
  margin-top:10px;
  font-size:14px;
  font-family:Arial;
  color:rgba(51,51,51,1);
  cursor: pointer;
`
const CountTag = styled(Tag)`
  width:30px;
  height:30px;
  background:rgba(255,255,255,1);
  border:1px solid rgba(241,241,241,1);
  opacity:1;
  cursor: pointer;
  text-align: center;
  font-size: 24px;
`