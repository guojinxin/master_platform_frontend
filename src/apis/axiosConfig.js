import axios from 'axios'
import {message} from "antd";
import {awsSignOut, currentSession} from "./auth";

axios.interceptors.request.use(async function (config) {
  // 从这里去取最新的jwt_token
  let token = localStorage.getItem('token');
  const jwtToken = await currentSession();
  if (jwtToken) {
    token = jwtToken;
    localStorage.setItem('token', jwtToken);
  }
  if (token != null) {
    config.headers.Authorization = token
  }
  if (config.method === "post") {
    config.data = {
      ...config.data,
    }
  }
  if (config.method === "get") {
    config.params = {
      ...config.params,
    }
  }
  return config
})

axios.interceptors.response.use(
  response => {
    return response
  },
  async error => {
    if (error.response) {
      switch (error.response.status) {
        case 401:
          // 返回 401 清除token信息并跳转到登录页面
          localStorage.clear();
          await awsSignOut();
          window.location.replace('/signIn');
          message.info('Invalid User');
          break;
        default:
          break;
      }
    }
    return Promise.reject(error.message)   // 返回接口返回的错误信息
  })

axios.defaults.headers.post['Content-Type'] = 'application/json;charset=utf-8';
