import Amplify, {Auth} from "aws-amplify";
import {END_POINT} from "../utils/constants";
import axios from "axios";

const {REACT_APP_AWS_CLIENT_ID, REACT_APP_AWS_USER_POOL_REGION, REACT_APP_AWS_USER_POOL_ID} = process.env
Amplify.configure({
  Auth: {
    region: REACT_APP_AWS_USER_POOL_REGION,
    userPoolId: REACT_APP_AWS_USER_POOL_ID,

    // OPTIONAL - Amazon Cognito Web Client ID (26-char alphanumeric string)
    userPoolWebClientId: REACT_APP_AWS_CLIENT_ID,

    // OPTIONAL - Enforce user authentication prior to accessing AWS resources or not
    mandatorySignIn: false,

    authenticationFlowType: "USER_PASSWORD_AUTH"
  }
});

export const awsSignIn = async function ({email, password}) {
  return Auth.signIn(email, password);
};

export const currentSession = async function () {
  try {
    const result = await Auth.currentSession();
    const jwtToken = result.getIdToken()
      .getJwtToken()
    return jwtToken
  } catch (e) {
    console.log(e)
    return ""
  }
}

export const awsSignOut = async function () {
  await Auth.signOut()
};

export const awsSignUp = async function ({email, password}) {
  return Auth.signUp({username: email, password, attributes: {email}});
};
export const forgetPassword = async function (username) {
  return Auth.forgotPassword(username);
};

export const forgetPasswordSubmit = async function (email, code, newPassword) {
  return Auth.forgotPasswordSubmit(email, code, newPassword);
};

export const confirmSignUp = async function (email, code) {
  return Auth.confirmSignUp(email, code);
};

export const resendEmailVerificationCode = function (email) {
  return Auth.resendSignUp(email);
};

export const changePassword = async function (oldPassword, newPassword) {
  return Auth.currentAuthenticatedUser().then(user => {
    return Auth.changePassword(user, oldPassword, newPassword);
  });
};

export const getUserInfoByUserId = async () => {
  return await axios.get(`${END_POINT}/user/getUserInfoByUserId`)
}

export const resetUserName = async (userName) => {
  return await axios.post(`${END_POINT}/user/resetUserName`, {userName})
}

export const updateUserBasicInfo =async (userInfo)=>{
  return await axios.put(`${END_POINT}/user/updateUserId`,userInfo)
}
