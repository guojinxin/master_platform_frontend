import {message} from 'antd';
import {MessageContent, MessageError} from "../components/CommonComponents/MessageContent";
import React from "react";
import {ImageCropperComponent} from "../components/CommonComponents/ImageCropperComponent";

export const beforeUploadCourseCover = ({file, openModal}) => {
  const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
  if (!isJpgOrPng) {
    message.error({
      icon: <span/>,
      content: <MessageContent><MessageError>You can only upload JPG/PNG file!</MessageError></MessageContent>,
    })
  }
  const isLt2M = file.size / 1024 / 1024 < 5;
  if (!isLt2M) {
    message.error({
      icon: <span/>,
      content: <MessageContent><MessageError>Image must smaller than 5MB!</MessageError></MessageContent>,
    })
  }
  if (openModal) {
    return new Promise((resolve, reject) => {
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = (e) => {
        openModal({
          content: <ImageCropperComponent src={e.target.result} onOk={((dataUrl) => {
            resolve(dataUrl)
          })}/>
        })
      }
    })
  } else {
    return isJpgOrPng && isLt2M;
  }
}

export const beforeUploadAvatar = ({file, openModal}) => {
  const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
  if (!isJpgOrPng) {
    message.error({
      icon: <span/>,
      content: <MessageContent><MessageError>You can only upload JPG/PNG file!</MessageError></MessageContent>,
    })
  }
  const isLt2M = file.size / 1024 / 1024 < 5;
  if (!isLt2M) {
    message.error({
      icon: <span/>,
      content: <MessageContent><MessageError>Image must smaller than 5MB!</MessageError></MessageContent>,
    })
  }
  if (openModal) {
    return new Promise((resolve, reject) => {
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = (e) => {
        openModal({
          content: <ImageCropperComponent
            aspectRatio={1}
            src={e.target.result}
            onOk={((dataUrl) => {
              resolve(dataUrl)
            })}/>
        })
      }
    })
  } else {
    return isJpgOrPng && isLt2M;
  }
}
