import html2pdf from 'html2pdf.js';

export const exportPDFFile = ({pdfName,pdfData}) => {
  // 导出配置
  const opt = {
    margin: 1,
    filename: pdfName,
    image: { type: 'jpeg', quality: 0.98 }, // 导出的图片质量和格式
    html2canvas: { scale: 2, useCORS: true }, // useCORS很重要，解决文档中图片跨域问题
    jsPDF: { unit: 'in', format: 'letter', orientation: 'portrait' },
  };
  if (pdfData) {
    html2pdf().set(opt).from(pdfData).save(); // 导出
  }
};
