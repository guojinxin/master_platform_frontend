import axios from 'axios';
import {END_POINT} from "./constants";

export const downloadFile = ({attachmentName, attachmentKey}) => {
  if (attachmentKey) {
    const fileNames = attachmentKey.split(".");
    const fileName =
      attachmentName +
      "." +
      fileNames[fileNames.length - 1];
    try {
      axios.get(`${END_POINT}/course/attachment`, {
        responseType: 'blob',
        params: {bucketKey: attachmentKey, fileName}
      }).then((res) => {
        let url = window.URL.createObjectURL(new Blob([res.data]));
        let link = document.createElement("a");
        link.style.display = "none";
        link.href = url;
        link.setAttribute("download", fileName);
        document.body.appendChild(link);
        console.log(res);
        link.click();
      })
    } catch (e) {
        console.log(e)
    }}
  }
