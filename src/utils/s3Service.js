import {config, S3} from 'aws-sdk'

export const createS3 = (cfg) => {
  const setting = {
    accessKeyId: cfg.AccessKeyId,
    secretAccessKey: cfg.SecretAccessKey,
    sessionToken: cfg.SessionToken,
  }
  config.update(setting);
  config.region="us-east-1";

  const s3 = new S3({
    apiVersion: 'latest',
  });
  return s3;
};

// 自定义S3上传
export const uploadFile = (s3Config, bucket, key, body) => {
  const s3 = createS3(s3Config);
  const uploadPromise = new Promise((resolve, reject) => {
    s3.putObject(
      {
        Body: body.file,// 文件
        Bucket: bucket,// s3里面的bucket
        Key: key, // 上传的路径
      },
      (err, response) => {
        if (err) {
          reject(err)
        } else {
          resolve(response)
        }
      }
    ).on('httpUploadProgress', (progress) => {
      const percent = 100 * progress / progress.total;
      body.onProgress ? body.onProgress({percent}) : void 0;
      if (percent === 100 && body.onSuccess) {
        body.onSuccess()
      }
    }).on('httpError', (err) => {
      if (err && body.onError) {
        body.onError();
        reject(err);
      }
    })
  })
  return uploadPromise
}
