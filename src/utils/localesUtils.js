import en_US from './languageLibrary/en-US.json'
import zh_CN from './languageLibrary/zh-CN.json'

import intl from "react-intl-universal";
import 'intl/locale-data/jsonp/en.js';
import 'intl/locale-data/jsonp/zh.js';
import 'moment/locale/zh-cn';

import {find} from "lodash";
import {lang} from "moment";

export const ComponentsLocales = {
  'en-US': en_US,
  'zh-CN': zh_CN,
};


export const momentLanguage = {
  'en-US': "en",
  'zh-CN': 'zh-cn',
}

export const SUPPORT_LOCALES = [
  {
    name: "English",
    value: "en-US"
  },
  {
    name: "简体中文",
    value: "zh-CN"
  },
]

// 获取浏览器默认的语言
export const loadLocales = () => {
  let currentLocale = intl.determineLocale({
    urlLocaleKey: "lang",
    cookieLocaleKey: "lang"
  });
  if (!find(SUPPORT_LOCALES, {value: currentLocale})) {
    currentLocale = "en-US";
  }
  return currentLocale
}

export const BACKEND_LANGUAGE = {
  "en-US": 'en',
  'zh-CN': 'cn',
}

// 获取地址栏里面的地址

export const getLangFromUrl = () => {
  const reg = new RegExp("(^|&)lang=([^&]*)(&|$)");
  const result = window.location.search.substr(1).match(reg);
  let lang = '';
  if (result) {
    lang = decodeURIComponent(result[2])
  }
  if(!lang){
    lang = sessionStorage.getItem('lang')
  }
  switch (lang) {
    case "zh-CN":
    case "en-US":
      return lang
    default :
      return "zh-CN"
  }
}
