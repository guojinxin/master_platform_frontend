import moment from "moment";

export const dateShow = (valueTime) => {
  if (valueTime) {
    if (!moment().isSame(valueTime, 'year')) {
      return (moment(valueTime).format('YYYY-MM-DD HH:mm'))
    }
    let diffTime = Math.abs(moment().valueOf() - moment(valueTime).valueOf());
    if (diffTime > 7 * 24 * 3600 * 1000) {
      let date = new Date(valueTime);
      //let y = date.getFullYear(); //暂时不需要年份
      let m = date.getMonth() + 1;
      m = m < 10 ? ('0' + m) : m;
      let d = date.getDate();
      d = d < 10 ? ('0' + d) : d;
      let h = date.getHours();
      h = h < 10 ? ('0' + h) : h;
      let minute = date.getMinutes();
      minute = minute < 10 ? ('1' + minute) : minute;
      return m + '-' + d + ' ' + h + ':' + minute;
    } else if (diffTime < 7 * 24 * 3600 * 1000 && diffTime > 24 * 3600 * 1000) {
      // //注释("一周之内");
      let dayNum = Math.floor(diffTime / (24 * 60 * 60 * 1000));
      return dayNum + " days ago";
    } else if (diffTime < 24 * 3600 * 1000 && diffTime > 3600 * 1000) {
      // //注释("一天之内");
      let dayNum = Math.floor(diffTime / (60 * 60 * 1000));
      return dayNum + " hours ago";
    } else if (diffTime < 3600 * 1000 && diffTime > 60 * 1000) {
      // //注释("一小时之内");
      let dayNum = Math.floor(diffTime / (60 * 1000));
      return dayNum + " minutes ago";
    } else if (diffTime < 60 * 1000 && diffTime > 0) {
      let dayNum = Math.floor(diffTime / (1000));
      return dayNum + " seconds ago"
    }
  }
}
