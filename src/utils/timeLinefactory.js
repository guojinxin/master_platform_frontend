import {isEmpty} from "lodash";

export const getMinAbs = (numberArray, currentNumber) => {
  if (isEmpty(numberArray)) {
    return {}
  }
  let minAbsData = numberArray[0];
  let initialAbs = Math.abs(numberArray[0].timePoint - currentNumber);
  numberArray.forEach((number) => {
    const abs = Math.abs(number.timePoint - currentNumber);
    if (initialAbs > abs) {
      initialAbs = abs;
      minAbsData = number;
    }
  })
  return minAbsData;
}
