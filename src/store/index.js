import {applyMiddleware, compose, createStore} from "redux";
import {appReducer} from "../reducers/index";
import thunk from 'redux-thunk'
import {createLogger} from 'redux-logger'
import {routerMiddleware} from 'connected-react-router';
import {createBrowserHistory} from 'history';
import {persistReducer, persistStore} from 'redux-persist'
import storage from 'redux-persist/lib/storage';
import reduxReset from 'redux-reset'

const middleware = [];
const logger = createLogger({});
export const history = createBrowserHistory();
const routeMiddleware = routerMiddleware(history);
/**
 * 想要什么中间件，push到这里面来
 * */
middleware.push(thunk);
if (process.env.REACT_APP_ENV !== 'production') {
  middleware.push(logger);
}
middleware.push(routeMiddleware);

const persistConfig = {
  key: 'root',
  storage,
  blacklist: ['socket', 'modal','message',"createCourse"],
};
const myPersistReducer = persistReducer(persistConfig, appReducer(history))

export const appStore = createStore(
  myPersistReducer,
  {},
  compose(applyMiddleware(...middleware),reduxReset()),
);

export const persistor = persistStore(appStore)
