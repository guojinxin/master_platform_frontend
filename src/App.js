import React from "react";
import { connect, Provider } from "react-redux";
import { Route, Switch } from "react-router-dom";
import { ConnectedRouter } from "connected-react-router";
import ReactGA from "react-ga";
import { appStore, history, persistor } from "../src/store";
import { routers } from "./routes";
// import { Chat } from "./components/ChatComponents/Chat";
import "./apis/axiosConfig";
import { ScrollToTop } from "./components/ScrollToTop";
import { message, Modal, Spin } from "antd";
import { NotFoundPage } from "./components/CommonComponents/NotFoundPage";
import { closeModal } from "../src/modules/homePage/actions/modalActions";
import { PersistGate } from "redux-persist/lib/integration/react";
import {
  FrameComponent,
  ContentFrame,
} from "./modules/homePage/components/FrameComponent";

if (process.env.REACT_APP_ENV === "production") {
  console.log = () => {};
  console.warn = () => {};
  console.info = () => {};
}

class App extends React.Component {
  componentDidMount() {
    ReactGA.initialize("UA-164655924-2");
    history.listen((location) => {
      ReactGA.set({ page: location.pathname });
      ReactGA.pageview(location.pathname);
    });
  }
  render() {
    return (
      <Provider store={appStore}>
        <PersistGate loading={null} persistor={persistor}>
          <RouterComponent history={history} />
        </PersistGate>
      </Provider>
    );
  }
}

class _RouterComponent extends React.Component {
  componentDidMount() {
    message.config({
      top: 0,
      duration: 2,
    });
    Spin.setDefaultIndicator(
      <img src={require("../src/assets/loading.gif")} alt="loading" />
    );
  }

  render() {
    const { modalVisible, modalContent, modalTitle } = this.props.modal;
    return (
      <div>
        <ConnectedRouter history={this.props.history}>
          <ScrollToTop>
            <FrameComponent needChangeIconColor={true}>
                <Switch>
                  {routers.map(({ path, exact, component: Comp }) => {
                    return (
                      <Route
                        key={path}
                        path={path}
                        exact={exact}
                        render={(props) => {
                          return <Comp {...props} />;
                        }}
                      />
                    );
                  })}
                  <Route component={NotFoundPage} />
                </Switch>
            </FrameComponent>
          </ScrollToTop>
          {/* {this.props.token && <Chat />} */}
        </ConnectedRouter>
        <Modal
          title={modalTitle}
          visible={modalVisible}
          onCancel={this.props.closeModal}
          footer={null}
        >
          {modalContent}
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    modal: state.modal,
  };
};

const RouterComponent = connect(mapStateToProps, { closeModal })(
  _RouterComponent
);
export default App;
