import {combineReducers} from "redux";
import {modalReducer} from "../modules/homePage/reducers/modalReducer";

import {connectRouter} from "connected-react-router";
export const appReducer = history =>
  combineReducers({
    router: connectRouter(history),
    modal: modalReducer,
  });
