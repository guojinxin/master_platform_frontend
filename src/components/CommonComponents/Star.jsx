import React from "react";
import {Rate} from "antd";

export const Star = ({selectStar, score}) => {
  return (
    <Rate
      value={score}
      disabled={!!!selectStar}
      onChange={(score) => {
        selectStar && selectStar(score)
      }}/>
  )
}
