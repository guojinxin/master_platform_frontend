import styled from "styled-components";
import {Tabs} from "antd";
import {PRIMARY_COLOR} from "../../utils/constants";

export const CustomTabs = styled(Tabs)`
  .ant-tabs-tab{
    padding:12px 0;
  }
  .ant-tabs-bar{
    border-width:0 !important;
    color:#999;
    font-size:17px;
    margin-left:37px;
  }
  .ant-tabs-ink-bar{
    background-color:${PRIMARY_COLOR} !important;
    height:11px;
    width:11px !important;
    border-radius:50%;
    left:38px !important;
  }
  .ant-tabs-tab-active{
    color:#333333 !important;
    font-size:24px !important;
  }
`
export const TabPaneText = styled.div`
  font-size:18px;
  min-width:80px;
  display:flex;
  justify-content:center;
`
