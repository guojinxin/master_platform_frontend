import {Icon, Progress} from "antd";
import React from "react";
import styled from "styled-components";

const UploadProgress = ({progress}) => {
  return (
    <Progress
      style={{margin: "0 20px"}}
      strokeColor={{
        from: '#108ee9',
        to: '#F6A603',
      }}
      percent={parseFloat(progress.toFixed(2))}
      status="active"
    />
  )
}

export const UploadProgressComponent = ({fileUrl, progress, isOnProgress, fileComponent}) => {
  if (!isOnProgress && fileUrl) {
    return fileComponent
  } else if (!fileUrl && !isOnProgress) {
    return (
      <UploadIcon type="plus"/>
    )
  } else if (isOnProgress) {
    return (
      <UploadProgress progress={progress}/>
    )
  }
  return (
    <div/>
  )
}

export const UploadProgressUpdateComponent = ({fileUrl, progress, isOnProgress, fileComponent}) => {
  if (!isOnProgress && fileUrl) {
    return fileComponent
  } else if (!fileUrl && !isOnProgress) {
    return (
      <UploadIcon type="plus"/>
    )
  } else if (isOnProgress) {
    return (
      <UploadProgress progress={progress}/>
    )
  }
  return (
    <div/>
  )
}
const UploadIcon = styled(Icon)`
  color: #f1f1f1;
  font-size: 40px !important;
`;
