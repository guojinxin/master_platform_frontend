import React, {Component} from 'react';
import Swiper from "swiper";
import 'swiper/css/swiper.min.css';
import styled from 'styled-components';

export class SwiperComponent extends Component {
  componentDidMount() {
    const {swiperId, isShowPagination, onSlideChange, swiperData} = this.props
    const params = {
      effect: 'coverflow',
      loop: true,
      grabCursor: true,
      centeredSlides: true,
      slidesPerView: 'auto',
      loopedSlides:7,
      slidesOffsetBefore:0,
      slidesOffsetAfter:0,
      //slideToClickedSlide: true,
      preventClicksPropagation: true,
      coverflowEffect: {
        rotate: 0,
        stretch: 450,
        depth: 650,
        modifier: 0.5,
        slideShadows: false,
      },
      pagination: {},
    }
    if (isShowPagination) {
      params.pagination = {
        el: '.swiper-pagination',
      }
    }
    const mySwiper = new Swiper(`.swiper-container-${swiperId}`, params);
    mySwiper.on('slideChange', () => {
      onSlideChange && onSlideChange(mySwiper.activeIndex % swiperData.length)
    })
  }

  render() {
    const {swiperId, swiperData, renderItem} = this.props;
    
    return (
      <SwiperContainer>
        <div className={`swiper-container-${swiperId} swiper-container`}>
          <div className="swiper-wrapper">
            {swiperData.map((item, index) => {
              return (
                <div className={'swiper-slide'} key={index}>
                  {renderItem(item, index)}
                </div>)
            })}
          </div>
          <div className="swiper-pagination"/>
        </div>
      </SwiperContainer>

    );
  }
}

const SwiperContainer = styled.div`
  width: 100%;
    .swiper-container{
      width: 100%
    }
    .swiper-slide {
      background-position: center;
    }
`
