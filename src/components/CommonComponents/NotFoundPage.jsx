import React from "react";
import styled from 'styled-components';
import {PRIMARY_COLOR} from "../../utils/constants";

export const NotFoundPage = () => {
  return (
    <Container>
      <Title>404</Title>
      <RouteErrorImage src={require('./assets/404.png')}/>
    </Container>
  )
}
const Container = styled.div`
  width:100%;
  height:100%;
  min-height:100vh;
  min-width:100vw;
  display:flex;
  align-items:center;
  justify-content:center;
  flex-direction:column;
`
const Title = styled.span`
  font-size:100px;
  font-weight:bold;
  color:${PRIMARY_COLOR}
`
const RouteErrorImage = styled.img`
  width:80%;
  height:auto;
`
