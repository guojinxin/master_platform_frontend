import React from "react";
import {Avatar, Button, Empty, Form, Popover} from "antd";
import styled from 'styled-components';
import {connect} from "react-redux";
import {findIndex} from "lodash";
import {closeModal, openModal} from "../../modules/homePage/actions/modalActions";
import {TextInputArea} from "./InputComponents";

export const AvatarWithPopover = ({userInfo, children}) => {
  return (
    <Popover
      content={<UserInfoContent userInfo={userInfo}/>}
      title={null}
      trigger={"click"}
      placement={"right"}
    >
      <span style={{cursor: "pointer"}}>
        {children}
      </span>
    </Popover>
  )
}

const _UserInfoContent = ({userInfo, ...props}) => {
  const {avatar, userName, userId, introduce = ""} = userInfo;
  const {friendList} = props.friends;
  const personalId = props.personalId;
  const showChat = findIndex(friendList, (o) => {
    return o.friendId === userId
  })
  const showFooter = userId !== personalId;
  return (
    <PopoverContainer>
      <UserNameContainer>
        <UserAvatar src={avatar}/>
        <UserName>{userName}</UserName>
      </UserNameContainer>
      {introduce ?
        <Introduce>{introduce}</Introduce> :
        <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description={"He/She didn't left any message"}/>
      }
      {showFooter && <ButtonContainer>
        {showChat < 0 ?
          <ChatButtonIcon
            src={require('./assets/icon_add_friend.png')}
            onClick={() => props.openModal({
              title: "Establish Friend Apply",
              content: <FriendApplyModalContent userInfo={userInfo}/>,
            })}/> :
          <ChatButtonIcon src={require('./assets/icon_chat_now.png')}
                          />
        }
      </ButtonContainer>}
    </PopoverContainer>
  )
}
const mapStateToProps = (state) => {
  return {
    friends: state.friends,
    personalId: state.auth.userInfo.userId,
  }
}
const UserInfoContent = connect(mapStateToProps, {openModal})(_UserInfoContent);
const FormItem = Form.Item;
const _FriendApplyModalContent = ({userInfo, ...props}) => {
  const {getFieldDecorator} = props.form
  return (
    <FriendApplyContainer>
      <UserAvatar src={userInfo.avatar}/>
      <Hint>You will send to <UserName>{userInfo.userName}</UserName> an apply message</Hint>
      <TextAreaContainer>
        <Form>
          <FormItem hasFeedback>
            {getFieldDecorator(`applyMessage`)(
              <TextInputArea
                placeholder={"Apply Message"}
              />
            )}
          </FormItem>
          <ConfirmButtonConatiner>
            <ApplyButton type={'primary'}>Confirm</ApplyButton>
          </ConfirmButtonConatiner>
        </Form>
      </TextAreaContainer>
    </FriendApplyContainer>
  )
}
const FriendApplyModalContent = connect(null, {
  closeModal,
})(Form.create({name: "friendApply"})(_FriendApplyModalContent))
const UserNameContainer = styled.div`
  display:flex;
  align-items:center;
`
const UserAvatar = styled(Avatar)`
  height:50px;
  width:50px;
`
const UserName = styled.span`
  font-size:18px;
  color:#333;
  margin-left:10px;
`
const ButtonContainer = styled.div`
  width:100%;
  display:flex;
  justify-content:flex-end;
`
const ChatButtonIcon = styled.img`
  height:40px;
  width:40px;
  cursor:pointer;
`
const PopoverContainer = styled.div`
  width:280px;
`
const Introduce = styled.div`
  font-size:14px;
  padding:10px 0 0;
`
const FriendApplyContainer = styled.div`
  display:flex;
  align-items:center;
  flex-direction:column;
`
const ApplyButton = styled(Button)`
  height:40px;
`
const Hint = styled.div`
  
`
const TextAreaContainer = styled.div`
  width:100%;
  margin-top:10px;
`
const ConfirmButtonConatiner = styled.div`
  display:flex;
  justify-content:flex-end;
`
