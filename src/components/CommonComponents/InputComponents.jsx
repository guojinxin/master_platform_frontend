import React, { Component } from 'react';
import styled from 'styled-components';
import { Input, Radio, Select } from "antd";
import { PRIMARY_COLOR } from "../../utils/constants";

export const TitleWithInput = ({ title, children, extraText = "" }) => {
  return (
    <Container>
      <TitleContainer>
        <Title>{title}</Title>
        <ExtraContainer>
          <ExtraText>{extraText}</ExtraText>
        </ExtraContainer>
      </TitleContainer>
      {children}
    </Container>
  )
}


export class TitleWithOption extends Component {
  state = {
    isFree: false,
  }
  onChange = (e) => {
    const { formEntity } = this.props
    const isFree = e.target.value
    this.setState({
      isFree,
    });
    if (isFree) {
      formEntity && formEntity.setFieldsValue({ price: "0" })
    }
  }

  render() {
    const { title, children } = this.props;
    return (
      <Container>
        <TitleContainer>
          <Title>{title}</Title>
          <ExtraContainer>
            <Radio.Group onChange={this.onChange} value={this.state.isFree}>
              <Radio value={true}>Free</Radio>
              <Radio value={false}>Charge</Radio>
            </Radio.Group>
          </ExtraContainer>
        </TitleContainer>
        {children}
      </Container>
    )
  }
}

// export class UploadFrom extends Component {
//   state = {
//     isFree: false,
//   }
//   onChange = (e) => {
//     const { formEntity } = this.props
//     const isFree = e.target.value
//     this.setState({
//       isFree,
//     });
//     if (isFree) {
//       formEntity && formEntity.setFieldsValue({ price: "0" })
//     }
//   }

//   render() {
//     const { title, children } = this.props;
//     return (
//       <UpdateContainer>
//         <TitleContainer>
//           <Title>{title}</Title>
//           <ExtraContainer>
//             <Radio.Group onChange={this.onChange} value={this.state.isFree}>
//               <Radio value={true}>Free</Radio>
//               <Radio value={false}>Charge</Radio>
//             </Radio.Group>
//           </ExtraContainer>
//         </TitleContainer>
//         {children}
//       </UpdateContainer>
//     )
//   }
// }

const Container = styled.div`
  margin-bottom:20px;
  .ant-form-item-children  {
    width: 100% !important;
  }

  .ant-upload{
    padding:0;
    background:#fff;
    box-shadow:0px 3px 6px rgba(0,0,0,0.16);
    border-radius:10px;
  }
  .ant-upload-drag{
      border:0 !important;
    }
  
// `

const ExtraText = styled.span`
  font-size:14px;
  font-weight:400;
  color:rgba(153,153,153,1);
`
const TitleContainer = styled.div`
  display:flex;
  margin:0 0 15px 20px;
  align-items:center;
`
const Title = styled.div`
  color:#333;
  font-size:16px;
`
const ExtraContainer = styled.div`
  margin-left:100px;
`
export const TextInput = styled(Input)`
  height:40px;
  box-shadow:0px 3px 6px rgba(0,0,0,0.16);
  border-radius:10px;
  padding-left:20px;
  border:0;
`
export const SelectCategory = styled(Select)`
  width:100%;
  .ant-select-selection{
   box-shadow:0px 3px 6px rgba(0,0,0,0.16);
    border-radius:10px;
    padding-left:20px;
    height:40px;
    border:0;
    .ant-select-selection__rendered{
      display:flex;
      align-items:center;
      height:100%;
    }
    .ant-select-arrow{
      color:${PRIMARY_COLOR}
    }
  }
`
export const CustomSelect = styled(Select)`
  width:100%;
  .ant-select-selection{
    box-shadow:0px 3px 6px rgba(0,0,0,0.16);
    border-radius:10px;
    padding-left:20px;
    height:40px;
    border:0;
    .ant-select-selection__rendered{
      display:flex;
      height:100%;
      align-items:center;
    }
    .ant-select-selection__choice{
      color:#fff;
      background-color:${PRIMARY_COLOR};
      border-radius:12px;
      .ant-select-selection__choice__remove{
        color:#fff;
      }
    }
   }
`
export const TextInputArea = styled(Input.TextArea)`
  box-shadow:0px 3px 6px rgba(0,0,0,0.16);
  border-radius:10px;
  padding-left:20px;
  border:0;
  resize:none;
  height:141px !important;
`
