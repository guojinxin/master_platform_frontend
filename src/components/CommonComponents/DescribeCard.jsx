import React from 'react';
import {Card} from 'antd';
import styled from "styled-components";

export const DescribeCard = ({commentContent}) => {
  return (
    <CardView >
      <DescriptionContainer>
        <Description>{commentContent}</Description>
      </DescriptionContainer>

    </CardView>
  )
}

const CardView = styled(Card)`
  background:rgba(255,255,255,1);
  box-shadow:3px 3px 6px rgba(0,0,0,0.16);
  border-radius:20px;
  margin:10px;
  height:200px;
`

const DescriptionContainer = styled.div`
  display:flex;
  justify-content:space-between;
`
const Description = styled.div`
  color:rgba(102, 102, 102, 1);
  font-size:14px;
  min-height:120px;
  padding-right:5px;
`
