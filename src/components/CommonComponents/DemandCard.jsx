/**
 * Knowledge In Demand Card Component
 * */
import { Card, Avatar } from 'antd'
import styled from 'styled-components';
import React from "react";
import { Star } from "./Star";
import { dateShow } from "../../utils/dateShowUtil";

const { Meta } = Card;
export const DemandCard = ({ _id, imageUrl, avatar, category, title, stars, times,viewCounts }) => {
  return (
    <ShadowCard
      hoverable
      cover={<Image alt={title} src={imageUrl} />}
    >
      <Meta
        title={
          <MetaTitle
            avatar={avatar}
            category={category}
            title={title}
            times={times}
            averageStar={stars}
            viewCounts={viewCounts}
          />
        }
      />
    </ShadowCard>
  )
}

const MetaTitle = ({ avatar, category, title, averageStar, times,viewCounts }) => {
  return (
    <MetaTitleContainer>
      <AvatarIcon src={avatar} />
      <DescriptionContainer>
        <TextContainer>
          <Category>{category} - </Category><Title>{title}</Title>
        </TextContainer>
        <TimeContainer>
          <EyeIcon src={require('./assets/buy.png')} />
          <ViewCountsTitle>{viewCounts}</ViewCountsTitle>
          {/* <Times>{dateShow(times)}</Times> */}
          {/* <StarContainer>
            <Star score={averageStar} />
          </StarContainer> */}
        </TimeContainer>
      </DescriptionContainer>
    </MetaTitleContainer>
  )
}

const Image = styled.img`
  width:100%;
  object-fit: cover;
  height:200px;
`
const ShadowCard = styled(Card)`
  width:100%;
  background:rgba(255,255,255,1);
  box-shadow:3px 3px 6px rgba(0,0,0,0.16);
  border-radius:20px;
  margin-bottom:10px;
  .ant-card-cover img{
    border-radius:20px 20px 0px 0px;
  }
`

const AvatarIcon = styled(Avatar)`
  height:50px;
  width:50px;
`
const DescriptionContainer = styled.div`
  padding-left:10px;
  width: calc(100% - 50px);
`
const Title = styled.span`
  color:#333333;
  font-size:14px;

`
const TextContainer = styled.span`
  white-space:normal;
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
  line-height:25px;
`
const Category = styled.span`
  color:#FFC400;
  font-size:14px;

`
const TimeContainer = styled.div`
  display:flex;
  align-items:center;
  margin-top:18px;
 
`
const EyeIcon = styled.img`
  height:14px;
  width:14px;
`
const Times = styled.span`
  color:#999;
  font-size:12px;
  margin-left:10px;
`
const MetaTitleContainer = styled.div`
  display:flex;
  width:100%;
`
const StarContainer = styled.div`
  margin-left:20px;
`
const ViewCountsTitle = styled.span`
margin-left: 8px;
  font-size:12px;
  color:rgba(153,153,153,1);
  opacity:1;

`
