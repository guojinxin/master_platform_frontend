/**
 * Knowledge In Demand Card Component
 * */
import { Card, Avatar, Progress } from 'antd'
import styled from 'styled-components';
import React from "react";
import { Link } from 'react-router-dom';

const { Meta } = Card;
export const MyCourseUploadCard = ({ imageUrl, avatar, category, title, times }) => {
  return (
    <ShadowCard
      hoverable
      cover={<Image alt={title} src={imageUrl} />}
    >
      <Meta
        title={
          <MetaTitle
            avatar={avatar}
            category={category}
            title={title}
            times={times}
          />
        }
      />
    </ShadowCard>
  )
}

const MetaTitle = ({ avatar, category, title, times }) => {
  return (
    <MetaTitleContainer>
        <DescriptionContainer>
          <TextContainer>
            <Category>{category} - </Category><Title>{title}</Title>
          </TextContainer>
          <TimeContainer>
            <EyeIcon src={require('./assets/icon_eye.png')} />
            <Times>{times}</Times>
            <EyeIcon src={require('./assets/icon_comment.png')} />
            <Times>{times}</Times>

          </TimeContainer>
        </DescriptionContainer>
    </MetaTitleContainer>
  )
}

const Image = styled.img`
  width:100%;
  object-fit: cover;
  max-height: 200px;
`
const ShadowCard = styled(Card)`
  background:rgba(255,255,255,1);
  box-shadow:3px 3px 6px rgba(0,0,0,0.16);
  border-radius:20px;
  .ant-card-cover img{
    border-radius:20px 20px 0px 0px;
  }
`


const DescriptionContainer = styled.div`
  width: 100%;
`
const Title = styled.span`
  color:#333333;
  font-size:16px;
`
const TextContainer = styled.span`
  white-space:normal;
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
  line-height:25px;
`
const Category = styled.span`
  color:#FFC400;
  font-size:16px;
`
const TimeContainer = styled.div`
  display:flex;
  align-items:center;
  margin-top:18px;
`

const MetaTitleContainer = styled.div`
  display:flex;
  width:100%;
`
const EyeIcon = styled.img`
  height:20px;
  width:20px;
  margin-left: 10%;
`

const Times = styled.span`
  color:#999;
  font-size:14px;
  padding-left: 3%;
`