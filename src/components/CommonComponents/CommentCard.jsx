import React from 'react';
import {Avatar, Card} from 'antd';
import styled from "styled-components";
import {Star} from "./Star";
import {dateShow} from "../../utils/dateShowUtil";
import {AvatarWithPopover} from "./AvatarWithPopover";

export const CommentCard = ({commentContent, score, userInfo, userId, publishedAt, selectStar, style = {}}) => {
  const {avatar, userName} = userInfo
  return (
    <CardView style={style}>
      <DescriptionContainer>
        <Description>{commentContent}</Description>
      </DescriptionContainer>
      <UserContainer>
        <RowContainer>
          <AvatarWithPopover userInfo={userInfo}>
            <AvatarIcon src={avatar}/>
          </AvatarWithPopover>
          <UserName>{userName}</UserName>
        </RowContainer>
        <RowContainer>
          <ReplySpan>Reply</ReplySpan>
          <Star score={score} selectStar={selectStar}/>
          <PublishedAt>{dateShow(publishedAt)}</PublishedAt>
        </RowContainer>
      </UserContainer>
    </CardView>
  )
}

const CardView = styled(Card)`
  background:rgba(255,255,255,1);
  box-shadow:3px 3px 6px rgba(0,0,0,0.16);
  border-radius:20px;
  margin:10px;
  height:200px;
`

const DescriptionContainer = styled.div`
  display:flex;
  justify-content:space-between;
`
const Description = styled.div`
  color:#999;
  font-size:14px;
  min-height:120px;
  padding-right:5px;

`
const AvatarIcon = styled(Avatar)`
  width:30px;
  height:30px;
`

const UserContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content:space-between;
`
const RowContainer = styled.div`
  display:flex;
  align-items: center;
  margin-top:10px;
`
const UserName = styled.div`
  color:#999999;
  font-size:12px;
  margin-left:15px;

`
const PublishedAt = styled.div`
  color:#999999;
  font-size:12px;
  text-align:center;
  margin-left:30px;

`
const ReplySpan =styled.span`
  font-size:12px;
  color:rgba(255,137,0,1);
  margin-right:20px;
`
