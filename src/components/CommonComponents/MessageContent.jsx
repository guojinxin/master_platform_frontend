import styled from "styled-components";

export const MessageContent = styled.div`
  width:100vw;
  height:80px;
  display:flex;
  justify-content:center;
  align-items:center;
  font-size:28px;
`
export const MessageSuccess = styled.span`
  color:#5EBC00;
`
export const MessageError = styled.span`
  color:#D81E06;
`
