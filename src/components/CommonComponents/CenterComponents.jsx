import styled from "styled-components";
import {Col} from "antd";

export const CenterCol = styled(Col)`
  display: flex;
  align-items:center;
  justify-content:center;
  flex-direction:column;
  padding-left:2%;
  padding-right:2%
  
`
