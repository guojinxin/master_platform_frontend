import React, {Component} from 'react'
import {Button} from 'antd'
import Cropper from 'react-cropper'
import 'cropperjs/dist/cropper.css'
import {connect} from "react-redux";
import {closeModal} from "../../modules/homePage/actions/modalActions";

class _ImageCropperComponent extends Component {
  _crop() {
  }

  handleOk = () => {
    //将裁剪的图片转成blob对象
    this.cropper.getCroppedCanvas().toBlob((blob) => {
      this.props.onOk(blob);
      this.props.closeModal();
    }, "image/png");
  }

  render() {
    const {src} = this.props;
    return (
      <div className="cropper-wrap">
        <Cropper
          ref={refs => this.cropper = refs}
          src={src}
          style={{height: 400, width: '100%'}}
          aspectRatio={this.props.aspectRatio || (8 / 5)}
          guides={false}
          preview=".uploadCrop"
          crop={this._crop.bind(this)}
        />
        <Button onClick={this.handleOk}>Confirm</Button>
      </div>
    );
  }
}

export const ImageCropperComponent = connect(null, {closeModal})(_ImageCropperComponent)
