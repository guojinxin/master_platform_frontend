import React, { Component, Button } from 'react';
import styled from 'styled-components';
import './css/components.scss';
import { Tag } from "antd";
export const BannerCard = ({ cover, category, title, link }) => {
  return (
    <CardContainer className={"banner-card"}>
      <a href={link}>
        <Cover src={cover} />
        <TextContainer>
          <Category><Distance>{"1.5km"}</Distance> - </Category><Title>{title}</Title>
        </TextContainer>
      </a>
    </CardContainer>
  )
}

const CardContainer = styled.div`
  position:relative;
  object-fit: cover;
  border-radius:20px;
`
const Cover = styled.img`
  height:100%;
  width:100%;
  
`
const Title = styled.span`
  color:#fff;
  font-size:18px;
`
const TextContainer = styled.div`
  width: 100%;
  height: 5rem;
  width：100% white-space: normal;
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: 1;
  -webkit-box-orient: vertical;
  position: absolute;
  bottom: 0px;
  background: rgba(0,0,0,1);
  border-bottom-left-radius: 20px;
  border-bottom-right-radius: 20px;
  padding-top: 20px;
  padding-left: 20px;
  opacity:1;
}`
const Category = styled.span`
  color:#FFC400;
  font-size:18px;
`

const Distance = styled(Tag)`
width:60px;
height:20px;
background:rgba(245,175,25,1);
opacity:1;
border-radius:12px;
font-size:12px;
font-family:Arial;
color:rgba(255,255,255,1);
text-align: center;
`