import {HomePage} from "../modules/homePage/HomePage";
import {ProductDetailPage} from "../modules/productDetailPage/ProductDetailPage";
import {UserSettingPage} from "../modules/userSettingPage/UserSettingPage";


export const routers = [
  {
    path: "/",
    component: HomePage,
    exact: true
  },
  {
    path: "/productDetail",
    component: ProductDetailPage,
    exact: true
  },
  {
    path: "/userSetting",
    component: UserSettingPage,
    exact: true
  }
];
